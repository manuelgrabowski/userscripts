
## Salesforce Training Wheels Userscript

### Effects

- [Customer Accounts] The **Account Owner** is replaced with a SFDC search URL for the account owner's name
    - Future version: If there is only one search result, link that SFDC user profile
- [Opportunities] Resolves the Zuora subscription ID for the opportunity and adds it as a Opportunity page field
    - Links to a Zuora subscription search for that subscription name
    - [Mechanism] Finds a Quote that was sent to Z-Billing, pulls the Subscription name from that quote

### Screenshots

![Screenshot showing the resolved Account Owner field](./screenshots/accountOwner.png "showing the resolved Account Owner field"){height=300px}
![Screenshot showing the resolved Zuora subscription name](./screenshots/oppExample.png "showing the resolved Zuora subscription name"){height=300px}