// ==UserScript==
// @name         Surface Customer Editing Flags
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       You
// @match        https://customers.gitlab.com/admin/customer/*
// @match        https://customers.gitlab.com/admin/customer/
// @match        https://customers.gitlab.com/admin/customer
// @match        https://customers.gitlab.com/admin/customer*
// @icon         https://www.google.com/s2/favicons?sz=64&domain=gitlab.com
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    function surfaceFlags() {
        const formControlHiddenInputs = document.querySelectorAll('.form-control[type="hidden"]');
        formControlHiddenInputs.forEach(input => {
          input.type = "checkbox";
          input.checked = (input.value === "true");
          const label = document.createElement("label");
          label.innerText = input.id;
          input.parentNode.insertBefore(label, input.nextSibling);

          input.addEventListener("change", function() {
            input.value = input.checked;
          });
        });
    }

    function checkIfEditPage() {
        const url = window.location.href;
        const endsWithEdit = url.endsWith("/edit");
        if(endsWithEdit) {
            surfaceFlags();
        } else {
            // Do nothing
        }
    }

    checkIfEditPage();

    window.onpopstate = function() {
        checkIfEditPage();
    };
})();
