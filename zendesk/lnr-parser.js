// ==UserScript==
// @name         [ZD] L&R Event Helper
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       You
// @match        https://gitlab.zendesk.com/agent/tickets/*
// @icon         https://www.google.com/s2/favicons?sz=64&domain=zendesk.com
// @grant GM_registerMenuCommand
// ==/UserScript==

(function() {
    'use strict';
        'use strict';
    GM_registerMenuCommand('Parse Ticket Events', buttonHandler);

    //<div class="event-container">

    function createEmailList(text) {
        // Regular expression to match email addresses
        const emailRegex = /\S+@\S+\.\S+/g;

        // Find all email addresses in the text
        const emailAddresses = text.match(emailRegex);

        const supportInfoContainer = document.createElement("div");
        supportInfoContainer.style.padding = "25px";
        supportInfoContainer.style.backgroundColor = "#eee";

        const supportInfoContainerHeader = document.createElement("h4");
        supportInfoContainerHeader.innerText = "Support Debug Info";
        supportInfoContainer.appendChild(supportInfoContainerHeader);

        // Create a new <ul> element
        const emailList = document.createElement("ul");
        supportInfoContainer.appendChild(emailList);

        // Iterate through each email address and create a new <li> element
        emailAddresses.forEach(email => {
            const emailParts = email.split("@");
            if(emailParts.length>1) {
                const emailLink = document.createElement("a");
                let domainPart = emailParts[1];

                emailLink.innerText = `Customer Portal Search: ${domainPart}`;
                emailLink.href = "https://customers.gitlab.com/admin/customer?model_name=customer&query=" + domainPart;

                const listItem = document.createElement("li");
                listItem.appendChild(emailLink);

                emailList.appendChild(listItem);
            }

        });

        const subjectElement = document.querySelector(`[data-test-id='ticket-pane-subject']`);
        if(subjectElement.value =="IR - SM - Cloud Licensing exemption"){
            parseExemptionIR(supportInfoContainer);
        }
        // Return the <ul> element
        return supportInfoContainer;
    }

    function parseExemptionIR(debugContainer) {
        //Get IR <ul>
        let formData = document.querySelectorAll("ul[dir='auto']");
        if(formData.length != 1) {
            let errorMessage = document.createElement("div");
            errorMessage.style.color = "crimson";
            errorMessage.innerText = `Tried to find IR form data list, found ${formData.length} candidates`;
            debugContainer.appendChild(errorMessage);
            return
        } else {
            formData = formData[0];
        }
        const liElements = formData.querySelectorAll("li");
        liElements.forEach(li => {
            if(li.innerText.indexOf("GitLab Version") != -1) {
                let versionParts = li.innerText.split(": ");
                if(versionParts.length > 1) {
                   let versionString = versionParts[1];
                   let versionComponents = versionString.split(".");
                   let versionOutput = document.createElement("div");

                   if (versionComponents.length != 3) {
                       versionOutput.style.color = "crimson";
                       versionOutput.innerText = "Provided GitLab Version appears malformed";
                   } else {
                       let majorVersion = parseInt(versionComponents[0]);
                       if(majorVersion >= 15) {
                           versionOutput.innerText = "✅ Offline Cloud License";
                       } else {
                           versionOutput.innerText = "✅ Legacy License";
                       }
                   }
                   debugContainer.appendChild(versionOutput);
                }
            }
        });

    }

    function checkElementList(elementList){
        const emailRegex = /\S+@\S+\.\S+/;

        const emailElements = [];
        elementList.forEach(element => {
            if (emailRegex.test(element.innerText)) {
                emailElements.push(element);
            }
        });

        console.log(emailElements);
        emailElements.forEach(element => {
            let emailList = createEmailList(element.innerText);
            element.parentNode.insertBefore(emailList, element);
        });
    }

    function buttonHandler() {
        let eventElements = document.querySelectorAll(".event-container");
        checkElementList(eventElements);
    }

})();
