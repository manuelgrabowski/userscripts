### Zendesk Scripts

There are two scripts available in this repository - otherwise check out Rene's [punk project](https://gitlab.com/rverschoor/punk) which has a larger extent of Zendesk userscripts.

- `Queue-Snapshot.js` - This summarises the currently open queue (see [this comment for an example of the output](https://gitlab.com/gitlab-com/support/support-team-meta/-/issues/5657#note_1645477527))
- `lnr-parser.js` - Not maintained, _do not use_.

#### Installing a script

1. Ensure you have a Userscript manager installed (such as TamperMonkey or ViolentMonkey)
1. Navigate to the file for the script you'd like to install, and then click the **Open Raw** button.